package com.example.user.service;

import com.example.common.domain.User;
import com.example.user.model.UserRequest;
import com.example.user.model.UserResponse;
import com.example.user.model.UserUpdateRequest;
import com.example.user.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final ModelMapper modelMapper;
    private final KafkaMsgPublisher kafkaMsgPublisher;

    @Value("${spring.kafka.topics.user-events}")
    private String userEventTopic;

    @Override
    @Transactional(readOnly = true)
    public UserResponse get(Long id) {
        return userRepository
                .findById(id)
                .map(user -> modelMapper.map(user, UserResponse.class))
                .orElseThrow(() -> new RuntimeException("user not found"));
    }

    @Override
    public UserResponse create(UserRequest request) {
        User user = modelMapper.map(request, User.class);
        UserResponse response = modelMapper.map(userRepository.save(user), UserResponse.class);
        kafkaMsgPublisher.publish(null,user,userEventTopic);
        return response;
    }

    @Override
    public UserResponse update(Long id, UserUpdateRequest request) {
        return userRepository.findById(id).map(user -> {
            modelMapper.map(request, user);
            UserResponse userResponse = modelMapper.map(userRepository.save(user), UserResponse.class);
            return userResponse;
        }).orElseThrow(() ->  new RuntimeException("user not found"));
    }

    @Override
    public void delete(Long id) {
        User user = userRepository
                .findById(id)
                .orElseThrow(() -> new RuntimeException("user not found"));
        userRepository.delete(user);
    }
}
