package com.example.user.service;

import com.example.user.model.UserRequest;
import com.example.user.model.UserResponse;
import com.example.user.model.UserUpdateRequest;

public interface UserService {

    UserResponse get(Long id);

    UserResponse create(UserRequest request);

    UserResponse update(Long id, UserUpdateRequest request);

    void delete(Long id);
}
