package com.example.cards.service;

import com.example.cards.repository.UserRepository;
import com.example.common.domain.User;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    @Override
    public void create(User request) {
        userRepository.save(request);
    }
}
