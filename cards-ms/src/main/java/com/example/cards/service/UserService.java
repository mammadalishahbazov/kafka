package com.example.cards.service;


import com.example.common.domain.User;

public interface UserService {

    void create(User request);

}
