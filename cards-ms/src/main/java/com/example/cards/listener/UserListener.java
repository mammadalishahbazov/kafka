package com.example.cards.listener;

import com.example.cards.service.UserService;
import com.example.common.domain.User;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Component
@Slf4j
@RequiredArgsConstructor
public class UserListener {

    private final UserService userService;

    @KafkaListener(id = "1",
            topics = "user-events",
            groupId = "card-ms-user-events",
            containerFactory = "kafkaJsonListenerContainerFactory")
    public void userListener(User user)  {
        log.info("{}",user);
        userService.create(user);
    }
}
