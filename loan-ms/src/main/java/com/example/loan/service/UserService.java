package com.example.loan.service;


import com.example.common.domain.User;

public interface UserService {

    void create(User request);

}
