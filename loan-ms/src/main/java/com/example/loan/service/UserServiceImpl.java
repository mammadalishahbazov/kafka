package com.example.loan.service;

import com.example.common.domain.User;
import com.example.loan.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    @Override
    public void create(User request) {
        userRepository.save(request);
    }
}
