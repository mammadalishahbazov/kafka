package com.example.loan.listener;

import com.example.common.domain.User;
import com.example.loan.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.annotation.PartitionOffset;
import org.springframework.kafka.annotation.TopicPartition;
import org.springframework.stereotype.Component;

@Component
@Slf4j
@RequiredArgsConstructor
public class UserListener {

    private final UserService userService;

    @KafkaListener(id = "1",
            topics = "user-events",
            groupId = "loan-ms-user-events",
            containerFactory = "kafkaJsonListenerContainerFactory")
    public void userListener(User user)  {
        log.info("{}",user);
        userService.create(user);
    }
}
